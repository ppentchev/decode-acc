# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Packaging metadata for the decode-acc library."""

import setuptools

setuptools.setup()

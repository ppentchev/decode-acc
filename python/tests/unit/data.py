# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test data for the accumulator and splitter tests."""

from __future__ import annotations

import itertools
from typing import NamedTuple


class Decoded(NamedTuple):
    """The result of decoding a couple of characters."""

    length: list[int]
    length_raw: list[int]
    length_lines: list[int]
    lines: list[int]


class DecodeCase(NamedTuple):
    """A test case for decoding a sequence of bytes."""

    data: list[int]
    enc: dict[str, Decoded]


TEST_DATA = [
    DecodeCase(
        data=[0x41, 0x21],
        enc={
            "us-ascii": Decoded(
                length=[1, 2],
                length_raw=[0, 0],
                length_lines=[1, 2],
                lines=[0, 0],
            ),
            "Latin-1": Decoded(
                length=[1, 2],
                length_raw=[0, 0],
                length_lines=[1, 2],
                lines=[0, 0],
            ),
            "windows-1251": Decoded(
                length=[1, 2],
                length_raw=[0, 0],
                length_lines=[1, 2],
                lines=[0, 0],
            ),
            "UTF-8": Decoded(
                length=[1, 2],
                length_raw=[0, 0],
                length_lines=[1, 2],
                lines=[0, 0],
            ),
        },
    ),
    DecodeCase(
        data=[0x2D, 0xD0, 0xB0, 0x20, 0xD0, 0xB1, 0x21, 0x0A],
        enc={
            "us-ascii": Decoded(
                length=[1, 1, -1, -1, -1, -1, -1, -1],
                length_raw=[0, 1, 1, 1, 1, 1, 1, 1],
                length_lines=[1, 1, -1, -1, -1, -1, -1, -1],
                lines=[0, 0, 0, 0, 0, 0, 0, 0],
            ),
            "Latin-1": Decoded(
                length=[1, 2, 3, 4, 5, 6, 7, 8],
                length_raw=[0, 0, 0, 0, 0, 0, 0, 0],
                length_lines=[1, 2, 3, 4, 5, 6, 7, 0],
                lines=[0, 0, 0, 0, 0, 0, 0, 1],
            ),
            "windows-1251": Decoded(
                length=[1, 2, 3, 4, 5, 6, 7, 8],
                length_raw=[0, 0, 0, 0, 0, 0, 0, 0],
                length_lines=[1, 2, 3, 4, 5, 6, 7, 0],
                lines=[0, 0, 0, 0, 0, 0, 0, 1],
            ),
            "UTF-8": Decoded(
                length=[1, 1, 2, 3, 3, 4, 5, 6],
                length_raw=[0, 1, 0, 0, 1, 0, 0, 0],
                length_lines=[1, 1, 2, 3, 3, 4, 5, 0],
                lines=[0, 0, 0, 0, 0, 0, 0, 1],
            ),
        },
    ),
]

TEST_DATA_LIST = list(
    itertools.chain(*[[(v.data, i[0], i[1]) for i in v.enc.items()] for v in TEST_DATA]),
)

ENCODINGS = sorted(set(itertools.chain(*[v.enc.keys() for v in TEST_DATA])))


class SplitCase(NamedTuple):
    """A test case for the "split into lines" classes."""

    data: list[int]
    eol: dict[str, list[int]]


TEST_SPLIT = SplitCase(
    data=[0x65, 0x0A, 0x33, 0x0D, 0x0D, 0x0A, 0x21, 0x0D, 0x22, 0x0A, 0x0D],
    eol={
        "none": [1, 1, 0, 1, 1, 0],
        "": [2, 2, 2, 2, 2, 1],
        "\n": [1, 3, 3, 1],
        "\r": [3, 0, 2, 2],
        "\r\n": [4, 5],
    },
)

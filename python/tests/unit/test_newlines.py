# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Test the text splitters in the newlines submodule."""

from __future__ import annotations

import itertools
from typing import NamedTuple

import pytest

from decode_acc import newlines

from . import data as test_data


def process_data(
    splitter: newlines.TextSplitter,
    *,
    skip: int = 0,
    as_string: bool = False,
) -> newlines.TextSplitter:
    """Run the test data through the specified splitter."""
    data = test_data.TEST_SPLIT.data[skip:]
    data_str = "".join([chr(char) for char in data])
    if as_string:
        splitter = splitter.add_string(data_str)
    else:
        for char in data_str:
            splitter = splitter.add(char)
    return splitter.add(None)


@pytest.mark.parametrize("as_string", [False, True])
def test_non_split(*, as_string: bool) -> None:
    """Test not splitting text into lines at all."""
    splitter = process_data(newlines.NullSplitter(), as_string=as_string)
    assert not splitter.buf
    assert len(splitter.lines) == 1
    assert len(splitter.lines[0]) == len(test_data.TEST_SPLIT.data)
    assert splitter.done


@pytest.mark.parametrize("as_string", [False, True])
def test_universal(*, as_string: bool) -> None:
    """Test the universal newlines splitter."""
    splitter = process_data(newlines.UniversalNewlines())
    assert [len(line) for line in splitter.lines] == test_data.TEST_SPLIT.eol["none"]

    splitter = process_data(newlines.UniversalNewlines(preserve=True), as_string=as_string)
    assert [len(line) for line in splitter.lines] == test_data.TEST_SPLIT.eol[""]


@pytest.mark.parametrize(
    ("eol", "as_string"),
    itertools.product(("\r", "\n", "\r\n"), (False, True)),
)
def test_fixed(*, eol: str, as_string: bool) -> None:
    """Test the fixed line terminator splitter."""
    splitter = process_data(newlines.FixedEOLSplitter(eol=eol), as_string=as_string)
    assert [len(line) for line in splitter.lines] == test_data.TEST_SPLIT.eol[eol]


class TSplitter(NamedTuple):
    """A text splitter along with a tag identifying it."""

    tag: int
    spl: newlines.TextSplitter


def test_eq() -> None:
    """Test splitters for equality."""
    splitters: list[list[TSplitter]] = [[], []]
    for skip in range(2):
        for _ in range(4):
            splitters[skip].append(TSplitter(1, process_data(newlines.NullSplitter(), skip=skip)))
            splitters[skip].append(
                TSplitter(
                    2,
                    process_data(newlines.UniversalNewlines(), skip=skip),
                ),
            )
            splitters[skip].append(
                TSplitter(
                    3,
                    process_data(
                        newlines.UniversalNewlines(preserve=True),
                        skip=skip,
                    ),
                ),
            )
            splitters[skip].append(
                TSplitter(
                    4,
                    process_data(newlines.FixedEOLSplitter(eol="\r"), skip=skip),
                ),
            )
            splitters[skip].append(
                TSplitter(
                    5,
                    process_data(newlines.FixedEOLSplitter(eol="\n"), skip=skip),
                ),
            )
            splitters[skip].append(
                TSplitter(
                    6,
                    process_data(newlines.FixedEOLSplitter(eol="\r\n"), skip=skip),
                ),
            )

    # First test that splitters within each set are pretty much equal
    for skip in range(2):
        for (
            (tag_first, first),
            (tag_second, second),
        ) in itertools.combinations(splitters[skip], 2):
            if tag_first == tag_second:
                assert first == second
            else:
                assert first != second

    # Now make sure that the splitters differ between sets
    for (_tag_first, first), (_tag_second, second) in itertools.product(splitters[0], splitters[1]):
        assert first != second


@pytest.mark.parametrize(
    ("eol_data", "as_string"),
    itertools.product(test_data.TEST_SPLIT.eol.items(), (False, True)),
)
def test_split(*, eol_data: tuple[str | None, list[int]], as_string: bool) -> None:
    """Test various splitting techniques."""
    (eol, lines) = eol_data
    if eol == "none":
        eol = None
    splitter = newlines.get_dwim_splitter(eol)
    assert not splitter.lines
    assert not splitter.done

    splitter = process_data(splitter, as_string=as_string)
    assert [len(line) for line in splitter.lines] == lines
    assert splitter.done

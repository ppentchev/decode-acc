# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Some basic tests for the DecodeAccumulator class."""

from __future__ import annotations

import pytest

import decode_acc
from decode_acc import newlines

from . import data as test_data


@pytest.mark.parametrize("encoding", test_data.ENCODINGS)
def test_str_repr(encoding: str) -> None:
    """Test some basic aspects of a newly-initialized accumulator."""
    acc = decode_acc.accumulator.DecodeAccumulator(encoding=encoding)
    assert acc.encoding == encoding
    assert not acc.buf
    assert not acc.decoded

    str_desc = str(acc)
    assert str_desc.startswith("DecodeAccumulator: ")
    assert f' encoding "{encoding}"' in str_desc
    assert " 0 raw bytes" in str_desc
    assert " 0 decoded characters" in str_desc
    assert " done: False" in str_desc

    str_repr = repr(acc)
    assert str_repr.startswith("DecodeAccumulator(")
    assert f"encoding='{encoding}'" in str_repr
    assert "decoded=''" in str_repr
    assert "done=False" in str_repr


@pytest.mark.parametrize(("tdata", "enc_name", "enc_data"), test_data.TEST_DATA_LIST)
def test_decode(tdata: list[int], enc_name: str, enc_data: test_data.Decoded) -> None:
    """Actually test the incremental decoding of characters.

    Actually test the accumulator's capabilities of incrementally
    decoding characters in the specified encoding, along with its
    internal state at each step.
    """
    data = bytes(tdata)
    acc = decode_acc.accumulator.DecodeAccumulator(encoding=enc_name)
    multiplier = 1
    for i in range(len(data)):
        old_repr = repr(acc)
        try:
            nacc = acc.add(data[i : i + 1])
        except UnicodeDecodeError:
            multiplier = -1
        assert repr(acc) == old_repr
        acc = nacc
        assert len(acc.decoded) == multiplier * enc_data.length[i]
        assert len(acc.buf) == enc_data.length_raw[i]
        assert not acc.done

    old_repr = repr(acc)
    (nacc, text) = acc.pop_decoded()
    assert repr(acc) == old_repr
    assert nacc.encoding == acc.encoding
    assert nacc.buf == acc.buf
    assert not nacc.decoded
    assert not nacc.done
    assert len(text) == multiplier * enc_data.length[-1]

    old_repr = repr(acc)
    nacc = acc.add(None)
    assert repr(acc) == old_repr
    assert nacc.encoding == acc.encoding
    assert nacc.buf == acc.buf
    assert nacc.decoded == acc.decoded
    assert nacc.done
    with pytest.raises(RuntimeError):
        nacc.add(b"a")


@pytest.mark.parametrize(("tdata", "enc_name", "enc_data"), test_data.TEST_DATA_LIST)
def test_split(tdata: list[int], enc_name: str, enc_data: test_data.Decoded) -> None:
    """Test incremental splitting during the incremental decoding."""
    data = bytes(tdata)
    acc = decode_acc.accumulator.DecodeAccumulator(encoding=enc_name)
    spl = newlines.UniversalNewlines()  # type: newlines.TextSplitter
    multiplier = 1
    for i in range(len(data)):
        try:
            (acc, decoded) = acc.add(data[i : i + 1]).pop_decoded()
        except UnicodeDecodeError:
            multiplier = -1
        spl = spl.add_string(decoded)
        assert len(spl.buf) == multiplier * enc_data.length_lines[i]
        assert len(spl.lines) == multiplier * enc_data.lines[i]
        assert not spl.done

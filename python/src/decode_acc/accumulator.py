# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Incrementally decode bytes into strings and lines."""

from __future__ import annotations

import dataclasses


@dataclasses.dataclass(frozen=True)
class DecodeAccumulator:
    r"""Incrementally decode bytes into strings.

    This class implements an incremental decoder: an object that may be
    fed bytes (one or several at a time) as they are e.g. read from
    a network stream or a subprocess's output, and that adds to a result
    string as soon as enough bytes have been accumulated to produce
    a character in the specified encoding.

    Note that DecodeAccumulator objects are immutable value objects:
    the add() method does not modify its invocant, but returns a new
    DecodeAccumulator object instead.

    Sample usage:

        while True:
            bb = subprocess.stdout.read(1024)
            if len(bb) == 0:
                break
            acc = acc.add(bb)
            assert(not acc.done)
            if acc.decoded:
                # at least one full line was produced
                (acc, text) = acc.pop_decoded()
                print(text, end='')

        acc.add(None)
        if acc.buf:
            print('Leftover bytes left in the buffer!', file=sys.stderr)

        if acc.splitter.buf:
            print('Incomplete line: ' + acc.splitter.buf)

        final = acc.add(None)
        assert(final.done)

    Sample usage with a text splitter, e.g. one from the newlines module:

        spl = newlines.get_dwim_splitter()

        # May be done each time a character is decoded, or just once at
        # the end, or anything in between.
        (acc, text) = acc.pop_decoded()
        spl = spl.add_string(text)

        # Finalize; we won't be needing the accumulator any more...
        spl = spl.add_string(acc.add(None).pop_decoded()[1])
        print('f{len(spl.lines)} lines and {len(spl.buf)} leftover characters')

        # If we don't care that there might be no EOL on the last line,
        # we might as well have finalized the splitter, too, to form
        # one last line:
        spl = spl.add(None)
        print(
            f'{len(spl.lines)} lines and '
            f'{len(spl.buf)} (should be 0) leftover characters'
        )
        assert len(spl.buf) == 0
    """

    encoding: str = "UTF-8"
    buf: bytes = b""
    decoded: str = ""
    done: bool = False

    def add(self, new_data: bytes | None) -> DecodeAccumulator:
        """Add bytes to the buffer, try to decode characters.

        If invoked with None as a parameter, finalizes the incremental
        encoding; no more bytes may be added in the future.
        """
        if new_data is None:
            return dataclasses.replace(self, done=True)

        if self.done:
            raise RuntimeError(repr(self))
        buf = self.buf + new_data
        try:
            decoded, buf = buf.decode(self.encoding), b""
        except UnicodeDecodeError as err:
            if err.end < len(buf):
                raise
            if err.start == 0:
                decoded = ""
            else:
                decoded, buf = buf[: err.start].decode(self.encoding), buf[err.start :]

        return dataclasses.replace(self, buf=buf, decoded=self.decoded + decoded, done=False)

    def pop_decoded(self) -> tuple[DecodeAccumulator, str]:
        """Break out (into a new object) any fully decoded characters.

        Return a tuple with two elements:
        - an object with the same incremental decoding state, but
          without the accumulated decoded characters
        - the text decoded so far
        """
        return dataclasses.replace(self, decoded=""), self.decoded

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return (
            f'DecodeAccumulator: encoding "{self.encoding}", '
            f"{len(self.buf)} raw bytes, "
            f"{len(self.decoded)} decoded characters, done: {self.done}"
        )

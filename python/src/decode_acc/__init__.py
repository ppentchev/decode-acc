# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Incrementally decode bytes into strings and lines.

See the documentation of the DecodeAccumulator class.
"""

from .accumulator import DecodeAccumulator  # noqa: F401


VERSION = "2.0.0"

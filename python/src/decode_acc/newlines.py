# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Separate a stream of text into lines."""

from __future__ import annotations

import dataclasses
import functools


@dataclasses.dataclass(frozen=True)
# class TextSplitter(metaclass=abc.ABCMeta):
class TextSplitter:
    """Base class for building text splitters.

    Test splitters are objects that are fed characters and spit out
    fully-formed lines.
    """

    buf: str = ""
    lines: list[str] = dataclasses.field(default_factory=list)
    done: bool = False

    def add(self, char: str | None) -> TextSplitter:
        """Process a character or, if None, finalize the splitting as needed.

        Note: this method only handles the None case; derived classes must
        override it to actually process characters and form lines.
        """
        if char is not None:
            raise NotImplementedError

        if self.buf:
            return dataclasses.replace(self, buf="", lines=[*self.lines, self.buf], done=True)

        return dataclasses.replace(self, done=True)

    def add_string(self, text: str) -> TextSplitter:
        """Process all the characters in the specified string."""
        return functools.reduce(lambda spl, char: spl.add(char), text, self)

    def pop_lines(self) -> tuple[TextSplitter, list[str]]:
        """Extract (into a new object) the accumulated full lines.

        Return a tuple with two elements:
        - an object with the same incomplete line buffer, but
          without the accumulated full lines
        - the full lines accumulated so far
        """
        if not self.lines:
            return (self, [])

        return (dataclasses.replace(self, lines=[]), self.lines)

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return (
            f"{type(self).__name__}: {len(self.lines)} lines + "
            f"{len(self.buf)} characters, done: {self.done}"
        )


@dataclasses.dataclass(frozen=True)
class UniversalNewlines(TextSplitter):
    """Simulate the Python traditional "universal newlines" behavior.

    Split a string into text lines in a manner similar to the file class's
    universal newlines mode: detect LF, CR/LF, and bare CR line terminators.
    """

    preserve: bool = False
    was_cr: bool = False

    def add(self, char: str | None) -> TextSplitter:  # noqa: PLR0911
        """Add a character to the buffer, and split out a line if needed.

        The line is split out depending on the character being CR or LF and on
        the previous state of the buffer (e.g. detecting CR/LF combinations).
        """

        def newline(eol: str) -> TextSplitter:
            """Turn the accumulated buffer into a new line."""
            line = self.buf + (eol if self.preserve else "")
            return dataclasses.replace(self, buf="", lines=[*self.lines, line], was_cr=False)

        if char is None:
            if self.was_cr:
                return newline("\r").add(char)
            return super().add(char)

        if self.done:
            raise RuntimeError(repr(self))
        if self.was_cr:
            if char == "\n":
                return newline("\r\n")
            return newline("\r").add(char)

        match char:
            case "\n":
                return newline("\n")

            case "\r":
                return dataclasses.replace(self, was_cr=True)

            case _:
                return dataclasses.replace(self, buf=self.buf + char)

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return (
            f"UniversalNewlines: {len(self.lines)} lines + "
            f"{len(self.buf)} characters, "
            f"done: {self.done}, preserve: {self.preserve}"
        )


@dataclasses.dataclass(frozen=True)
class NullSplitter(TextSplitter):
    """Do not split the text at all."""

    def add(self, char: str | None) -> TextSplitter:
        """Add a character to the buffer without any checks."""
        if char is None:
            return super().add(char)

        if self.done:
            raise RuntimeError(repr(self))
        return dataclasses.replace(self, buf=self.buf + char)

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return super().__str__()


@dataclasses.dataclass(frozen=True)
class FixedEOLSplitter(TextSplitter):
    r"""Split a string into lines using a fixed line separator.

    The separator may consist of more than one character, e.g. '\r\n'.
    """

    eol: str = "\n"
    in_eol: int = 0

    def add(self, char: str | None) -> TextSplitter:
        """Process a new character, possibly part of a line separator."""
        if char is None:
            if self.in_eol > 0:
                return dataclasses.replace(
                    self,
                    buf=self.buf + self.eol[: self.in_eol],
                    in_eol=0,
                ).add(char)
            return super().add(char)

        if self.done:
            raise RuntimeError(repr(self))
        if char == self.eol[self.in_eol]:
            if self.in_eol + 1 == len(self.eol):
                return dataclasses.replace(self, buf="", lines=[*self.lines, self.buf], in_eol=0)

            return dataclasses.replace(self, in_eol=self.in_eol + 1)
        if self.in_eol > 0:
            nspl = dataclasses.replace(
                self,
                buf=self.buf + self.eol[0],
                in_eol=0,
            )  # type: TextSplitter
            to_add = self.eol[1 : self.in_eol] + char
            for add_char in to_add:
                nspl = nspl.add(add_char)
            return nspl
        return dataclasses.replace(self, buf=self.buf + char)

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return (
            f"{type(self).__name__}: {len(self.lines)} lines + "
            f"{len(self.buf)} characters, "
            f"done: {self.done}, eol {self.eol!r}"
        )


def get_dwim_splitter(newline: str | None = None) -> TextSplitter:
    """Choose a splitter class in a manner similar to open().

    If None is passed for the newline parameter, universal newlines mode
    will be enabled and the line terminators will not be present in
    the returned lines.  If newline is an empty string, universal newlines
    mode is enabled, but the line terminators will be preserved.
    If newline has any other value, it is used as a line terminator and
    stripped from the returned lines.
    """
    if newline is None:
        return UniversalNewlines()
    if not newline:
        return UniversalNewlines(preserve=True)
    return FixedEOLSplitter(eol=newline)

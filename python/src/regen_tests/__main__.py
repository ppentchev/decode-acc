# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Load the test data and regenerate the Python test module."""

from __future__ import annotations

import dataclasses
import enum
import functools
import logging
import pathlib
import subprocess  # noqa: S404
import sys
import typing

import click
import jinja2
import tomli
import typed_format_version
import typedload.dataloader


if typing.TYPE_CHECKING:
    from typing import Final


class OutputMode(str, enum.Enum):
    """What kind of file we are generating."""

    PYTHON = "python"
    RUST = "rust"


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the regen-tests tool."""

    input_path: pathlib.Path
    output_mode: OutputMode
    output_path: pathlib.Path
    template_path: pathlib.Path
    log: logging.Logger


@dataclasses.dataclass(frozen=True)
class Decoded:
    """The results of decoding a single test case using a specific encoding."""

    length: list[int]
    length_raw: list[int]
    length_lines: list[int]
    lines: list[int]


@dataclasses.dataclass(frozen=True)
class DecodeCase:
    """A single test case for the decode accumulator."""

    data: list[int]
    enc: dict[str, Decoded]


@dataclasses.dataclass(frozen=True)
class SplitCase:
    """The test case for the text splitter classes."""

    data: list[int]
    eol: dict[str, list[int]]


@dataclasses.dataclass(frozen=True)
class TestData:
    """The actual test data in the definitions file."""

    decode: list[DecodeCase]
    split: list[SplitCase]


@dataclasses.dataclass(frozen=True)
class TestFormatVersion:
    """Specify the version of the test data definitions file format."""

    major: int
    minor: int


@dataclasses.dataclass(frozen=True)
class TestFormat:
    """The holder of the format version data."""

    version: TestFormatVersion


@dataclasses.dataclass(frozen=True)
class TestDataTop:
    """The full contents of the test definitions file."""

    format: TestFormat
    tdata: TestData


@functools.lru_cache
def build_logger(*, quiet: bool = False, verbose: bool = False) -> logging.Logger:
    """Build a logger that outputs to the standard output and error streams.

    Messages of level `WARNING` and higher go to the standard error stream.
    If `quiet` is false, messages of level `INFO` go to the standard output stream.
    If `verbose` is true, messages of level `DEBUG` also go to the standard error stream.
    """
    logger: Final = logging.getLogger("power_button_lock")
    logger.setLevel(logging.DEBUG if verbose else logging.WARNING if quiet else logging.INFO)
    logger.propagate = False

    diag_handler: Final = logging.StreamHandler(sys.stderr)
    if verbose:
        diag_handler.setLevel(logging.DEBUG)
        diag_handler.addFilter(lambda rec: rec.levelno != logging.INFO)
    else:
        diag_handler.setLevel(logging.WARNING)
    logger.addHandler(diag_handler)

    if not quiet:
        info_handler: Final = logging.StreamHandler(sys.stdout)
        info_handler.setLevel(logging.INFO)
        info_handler.addFilter(lambda rec: rec.levelno == logging.INFO)
        logger.addHandler(info_handler)

    return logger


def load_data(cfg: Config) -> TestData:
    """Load data from the TOML file."""
    cfg.log.debug("About to read the %(input_path)s input file", {"input_path": cfg.input_path})
    try:
        contents = cfg.input_path.read_text(encoding="UTF-8")
    except (OSError, UnicodeDecodeError) as err:
        sys.exit(f"Could not parse the {cfg.input_path} input file as UTF-8 text: {err}")
    try:
        raw = tomli.loads(contents)
    except ValueError as err:
        sys.exit(f"Could not parse the {cfg.input_path} input file as TOML: {err}")
    try:
        fver = typed_format_version.get_version(raw)
    except ValueError as err:
        sys.exit(f"Could not get the format version of the {cfg.input_path} input file: {err}")
    cfg.log.debug(
        "Got input file version %(major)d.%(minor)d",
        {"major": fver.major, "minor": fver.minor},
    )
    if fver.as_version_tuple() != (1, 0):
        sys.exit(f"Expected input file {cfg.input_path} version 1.0, got {fver.major}.{fver.minor}")

    loader = typedload.dataloader.Loader(failonextra=True, pep563=True)
    try:
        top_data: TestDataTop = loader.load(raw, TestDataTop)
    except ValueError as err:
        sys.exit(f"Could not parse the input file {cfg.input_path}: {err}")

    tdata = top_data.tdata
    if len(tdata.split) != 1:
        sys.exit(f"Expected a single tdata.split element in {cfg.input_path}")

    cfg.log.debug("Read %(count)d decoder test cases", {"count": len(tdata.decode)})
    return tdata


def f_py_hex_array(values: list[int]) -> str:
    """Format an array of hexadecimal values."""
    return "[" + ", ".join(f"0x{value:02X}" for value in values) + "]"


def f_py_int_array(values: list[int]) -> str:
    """Format an array of hexadecimal values."""
    return "[" + ", ".join(str(value) for value in values) + "]"


def f_py_escape_separator(name: str) -> str:
    r"""Escape '\r' and '\n' in the separator string."""
    return name.replace("\r", "\\r").replace("\n", "\\n")


def generate_file(cfg: Config, tdata: TestData) -> None:
    """Read the Jinja template, generate the output file."""
    cfg.log.debug("Preparing the Jinja environment")
    jenv = jinja2.Environment(
        autoescape=False,  # noqa: S701  # we hereby solemnly promise to not render any HTML
        loader=jinja2.FileSystemLoader(cfg.template_path.parent),
        undefined=jinja2.StrictUndefined,
    )
    jenv.filters["py_hex_array"] = f_py_hex_array
    jenv.filters["py_int_array"] = f_py_int_array
    jenv.filters["py_escape_separator"] = f_py_escape_separator
    jvars = {"decode": tdata.decode, "split": tdata.split[0]}

    cfg.log.debug("Rendering the template")
    try:
        result = jenv.get_template(cfg.template_path.name).render(**jvars)
    except jinja2.TemplateError as err:
        sys.exit(f"Could not render the {cfg.template_path} template: {err}")

    cfg.log.debug(
        "Writing %(count)d characters to %(output_path)s",
        {"count": len(result), "output_path": cfg.output_path},
    )
    cfg.output_path.write_text(result, encoding="UTF-8")


def run_post_python(cfg: Config) -> None:
    """Reformat the Python source code using the 'reformat' Tox environment."""
    try:
        subprocess.check_call(
            ["tox", "-e", "reformat"],  # noqa: S607
            cwd=cfg.output_path.parent.parent,
            shell=False,  # noqa: S603
        )
    except (OSError, subprocess.CalledProcessError) as err:
        sys.exit(f"Could not run 'tox -e reformat': {err}")


def run_post_rust(cfg: Config) -> None:
    """Reformat the Rust source code using 'cargo fmt'."""
    try:
        subprocess.check_call(
            ["sp-cargo", "fmt"],  # noqa: S607
            cwd=cfg.output_path.parent.parent,
            shell=False,  # noqa: S603
        )
    except (OSError, subprocess.CalledProcessError) as err:
        sys.exit(f"Could not run 'sp-cargo fmt': {err}")


POST_HANDLERS = {
    OutputMode.PYTHON: run_post_python,
    OutputMode.RUST: run_post_rust,
}


@click.command(name="regen-tests")
@click.option(
    "-i",
    "--input-path",
    type=pathlib.Path,
    required=True,
    help="the path to the data.toml input file",
)
@click.option(
    "-m",
    "--output-mode",
    type=OutputMode,
    required=True,
    help="the type of file to generate",
)
@click.option(
    "-o",
    "--output-path",
    type=pathlib.Path,
    required=True,
    help="the path to the file to generate",
)
@click.option(
    "-t",
    "--template-path",
    type=pathlib.Path,
    required=True,
    help="the path to the template file to use",
)
@click.option(
    "-q",
    "--quiet",
    is_flag=True,
    help="quiet operation; only display warning and error messages",
)
@click.option("-v", "--verbose", is_flag=True, help="verbose operation; display diagnostic output")
def main(  # noqa: PLR0913  # we do accept a lot of command-line arguments
    *,
    input_path: pathlib.Path,
    output_mode: OutputMode,
    output_path: pathlib.Path,
    template_path: pathlib.Path,
    quiet: bool,
    verbose: bool,
) -> None:
    """Read the test data, generate the output file."""
    cfg = Config(
        input_path=input_path,
        output_mode=output_mode,
        output_path=output_path,
        template_path=template_path,
        log=build_logger(quiet=quiet, verbose=verbose),
    )
    run_post = POST_HANDLERS.get(cfg.output_mode)
    if run_post is None:
        sys.exit(f"No post-run handler defined for {cfg.output_mode.value}")

    tdata = load_data(cfg)
    generate_file(cfg, tdata)
    run_post(cfg)


if __name__ == "__main__":
    main()

# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { }
}:
pkgs.mkShell {
  buildInputs = [
    pkgs.gitMinimal
    pkgs.reuse
  ];
  shellHook = ''
    set -e
    reuse lint
    exit
  '';
}

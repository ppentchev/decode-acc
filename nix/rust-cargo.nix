# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { }
}:
pkgs.mkShell {
  buildInputs = [
    pkgs.cargo
    pkgs.clippy
    pkgs.rustc
    pkgs.rustfmt
  ];

  shellHook = ''
    set -e
    set -x

    target="$(rustc --version -v | awk '$1 == "host:" { print $2 }')"
    cargo fmt --check
    env NIX_ENFORCE_PURITY=0 cargo build --target "$target"
    env NIX_ENFORCE_PURITY=0 cargo test --target "$target"
    env NIX_ENFORCE_PURITY=0 sh run-clippy.sh -n -- --target "$target"

    set +x
    exit
  '';
}

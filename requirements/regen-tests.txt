# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

click >= 8, < 9
jinja2 >= 3, < 4
tomli >= 2, < 3
typed-format-version >= 0.1, < 0.2
typedload >= 2.16, < 3

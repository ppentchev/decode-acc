#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */
//! An incremental decoder of bytes into characters and lines
//!
//! The `decode-acc` library allows programs to parse input that comes in
//! a couple of bytes at a time into valid UTF-8 (or any other supported
//! encoding) strings.

use std::fmt::{Display, Formatter, Result as FmtResult};
use std::str;

use itertools::Itertools;
use thiserror::Error;

/// Format an array of bytes as [0xD1, 0x03].
fn dump_array(arr: &[u8]) -> String {
    format!(
        "[{bytes}]",
        bytes = arr.iter().map(|byte| format!("0x{byte:02X}")).join(", ")
    )
}

/// Errors that occur during the decoding.
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum DecodeError {
    /// The characters do not build up to a valid UTF-8 string.
    #[error("Invalid byte sequence: {0}")]
    DecodeFailure(String),

    /// The characters do not build to a complete UTF-8 string.
    #[error("Leftover bytes at finalization: {0}")]
    Incomplete(String),

    /// Something went really, really wrong...
    #[error("Internal decode-acc error: {0}")]
    Internal(String),
}

/// Parse input byte by byte, decode it into a string with the specified encoding.
#[derive(Debug, Default)]
pub struct DecodeAccumulator {
    /// The bytes accumulated so far, presumably an incomplete character.
    buf: Vec<u8>,

    /// The characters successfully decoded so far.
    decoded: String,

    /// Has this accumulator been finalized (i.e. has [`DecodeAccumulator::done`] been called)?
    done: bool,
}

impl Display for DecodeAccumulator {
    #[inline]
    fn fmt(&self, wr: &mut Formatter<'_>) -> FmtResult {
        write!(
            wr,
            "DecodeAccumulator(buf: {buf}, decoded: '{decoded}', done: {done})",
            buf = dump_array(&self.buf),
            decoded = self.decoded.escape_debug(),
            done = self.done
        )
    }
}

impl DecodeAccumulator {
    /// Return and remove the string decoded so far.
    #[inline]
    #[must_use]
    pub fn pop_decoded(&mut self) -> String {
        let res = self.decoded.drain(..).collect();
        res
    }

    /// Try to decode some more bytes.
    ///
    /// # Errors
    ///
    /// [`DecodeError::DecodeFailure`] on invalid data.
    #[allow(clippy::missing_inline_in_public_items)]
    pub fn add(&mut self, data: &[u8]) -> Result<usize, DecodeError> {
        self.buf.extend_from_slice(data);
        match str::from_utf8(&self.buf) {
            Ok(complete) => {
                self.decoded.push_str(complete);
                self.buf.clear();
                Ok(self.decoded.len())
            }
            Err(err) => {
                let rest = self.buf.split_off(err.valid_up_to());
                // SAFETY:
                // We just got err.valid_up_to(), did we not?
                let partial = unsafe { str::from_utf8_unchecked(&self.buf) };
                self.decoded.push_str(partial);
                self.buf = rest;

                match err.error_len() {
                    Some(error_len) => Err(DecodeError::DecodeFailure(dump_array(
                        self.buf.get(..error_len).ok_or_else(|| {
                            DecodeError::Internal(format!(
                                "error_len {error_len} buf len {buf_len}",
                                buf_len = self.buf.len()
                            ))
                        })?,
                    ))),
                    None => Ok(self.decoded.len()),
                }
            }
        }
    }

    /// Mark this accumulator as done.
    ///
    /// # Errors
    ///
    /// [`DecodeError::Incomplete`] if some bytes are left over.
    #[inline]
    pub fn done(&mut self) -> Result<usize, DecodeError> {
        self.done = true;
        if self.buf.is_empty() {
            Ok(self.decoded.len())
        } else {
            Err(DecodeError::Incomplete(dump_array(&self.buf)))
        }
    }

    /// Query whether this accumulator has been finalized.
    #[inline]
    #[must_use]
    pub const fn is_done(&self) -> bool {
        self.done
    }
}

#[cfg(test)]
mod tests;

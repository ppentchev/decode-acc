/*
 * SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
 * SPDX-License-Identifier: BSD-2-Clause
 */

#![allow(clippy::non_ascii_literal)]
#![allow(clippy::panic_in_result_fn)]
#![allow(clippy::print_stdout)]

use std::error::Error;

use super::{DecodeAccumulator, DecodeError};

use itertools::Itertools;
use thiserror::Error;

#[derive(Debug, Error)]
enum TestError {
    #[error("Unexpected buf len in {0}, expected {1}, got {2}")]
    BufLenError(String, usize, usize),

    #[error("Unexpected decoded len in {0}, expected {1}, got {2}")]
    DecodedLenError(String, usize, usize),

    #[error("Did not expect adding {0} to fail: {1}: {2}")]
    UnexpectedError(String, String, String),

    #[error("Did not expect adding {0} to succeed: {1}")]
    UnexpectedSuccess(String, String),

    #[error("Did not expect finalization to fail: {0}: {1}")]
    UnexpectedFinError(String, String),

    #[error("Did not expect finalization to succeed: {0}")]
    UnexpectedFinSuccess(String),
}

const TEST_INPUT: [u8; 4] = [0x41, 0xD1, 0x8F, 0x21];

const TEST_EMOJI: [u8; 7] = [0xE2, 0x98, 0x80, 0xEF, 0xB8, 0x8F, 0x0A];

fn add_good(
    acc: &mut DecodeAccumulator,
    bytes: &[u8],
    exp_buf: usize,
    exp_decoded: usize,
) -> Result<usize, TestError> {
    let res = acc.add(bytes).map_err(|err| {
        TestError::UnexpectedError(format!("{acc}"), super::dump_array(bytes), format!("{err}"))
    })?;

    if acc.buf.len() != exp_buf {
        return Err(TestError::BufLenError(
            format!("{acc}"),
            exp_buf,
            acc.buf.len(),
        ));
    }

    if acc.decoded.chars().count() != exp_decoded {
        return Err(TestError::DecodedLenError(
            format!("{acc}"),
            exp_decoded,
            acc.decoded.chars().count(),
        ));
    }

    Ok(res)
}

fn add_bad(
    acc: &mut DecodeAccumulator,
    bytes: &[u8],
    exp_buf: usize,
    exp_decoded: usize,
    exp_bad: &[u8],
) -> Result<(), TestError> {
    match acc.add(bytes) {
        Ok(_) => {
            return Err(TestError::UnexpectedSuccess(
                format!("{acc}"),
                super::dump_array(bytes),
            ))
        }
        Err(DecodeError::DecodeFailure(bad)) => {
            if super::dump_array(exp_bad) != bad {
                return Err(TestError::UnexpectedError(
                    format!("{acc}"),
                    super::dump_array(bytes),
                    format!(
                        "Got a DecodeFailure for {bad} instead of {exp_bad}",
                        exp_bad = super::dump_array(exp_bad)
                    ),
                ));
            }
        }
        Err(err) => {
            return Err(TestError::UnexpectedError(
                format!("{acc}"),
                super::dump_array(bytes),
                format!("{err}"),
            ))
        }
    };

    if acc.buf.len() != exp_buf {
        return Err(TestError::BufLenError(
            format!("{acc}"),
            exp_buf,
            acc.buf.len(),
        ));
    }

    if acc.decoded.chars().count() != exp_decoded {
        return Err(TestError::DecodedLenError(
            format!("{acc}"),
            exp_decoded,
            acc.decoded.chars().count(),
        ));
    }

    Ok(())
}

fn done_good(acc: &mut DecodeAccumulator) -> Result<usize, TestError> {
    acc.done()
        .map_err(|err| TestError::UnexpectedFinError(format!("{acc}"), format!("{err}")))
}

fn done_bad(acc: &mut DecodeAccumulator, exp_buf: &[u8]) -> Result<(), TestError> {
    match acc.done() {
        Ok(_) => Err(TestError::UnexpectedFinSuccess(format!("{acc}"))),
        Err(DecodeError::Incomplete(buf)) => {
            if buf == super::dump_array(exp_buf) {
                Ok(())
            } else {
                Err(TestError::UnexpectedFinError(
                    format!("{acc}"),
                    format!(
                        "Expected buf {exp_buf}, got {buf}",
                        exp_buf = super::dump_array(exp_buf),
                    ),
                ))
            }
        }
        Err(err) => Err(TestError::UnexpectedFinError(
            format!("{acc}"),
            format!("{err}"),
        )),
    }
}

#[test]
fn test_trivial() -> Result<(), Box<dyn Error>> {
    println!("\nStarting the trivial test");

    let mut acc = DecodeAccumulator::default();
    println!("acc {acc}");
    assert!(!acc.is_done());

    add_good(&mut acc, &TEST_INPUT[0..1], 0, 1)?;
    println!("acc {acc}");
    assert!(!acc.is_done());

    add_good(&mut acc, &TEST_INPUT[1..3], 0, 2)?;
    println!("acc {acc}");
    assert!(!acc.is_done());

    add_good(&mut acc, &TEST_INPUT[3..4], 0, 3)?;
    println!("{acc}");
    assert!(!acc.is_done());

    done_good(&mut acc)?;
    println!("{acc}");
    assert!(acc.is_done());

    let res = acc.pop_decoded();
    println!("{acc}");
    assert_eq!(res, "Aя!");

    Ok(())
}

#[test]
fn test_emoji() -> Result<(), Box<dyn Error>> {
    println!("\nStarting the emoji test");

    let mut acc = DecodeAccumulator::default();
    println!("{acc}");
    assert!(!acc.is_done());

    add_good(&mut acc, &TEST_EMOJI[0..4], 1, 1)?;
    println!("{acc}");
    assert!(!acc.is_done());

    add_good(&mut acc, &TEST_EMOJI[4..], 0, 3)?;
    println!("{acc}");
    assert!(!acc.is_done());

    done_good(&mut acc)?;
    println!("{acc}");
    assert!(acc.is_done());

    let res = acc.pop_decoded();
    println!("{acc}");
    assert_eq!(res, "\u{2600}\u{fe0f}\n");

    Ok(())
}

#[test]
fn test_incomplete() -> Result<(), Box<dyn Error>> {
    println!("\nStarting the incomplete test");
    let mut acc = DecodeAccumulator::default();
    println!("{acc}");
    assert!(!acc.is_done());

    add_good(&mut acc, &TEST_INPUT[0..2], 1, 1)?;
    println!("{acc}");
    assert!(!acc.is_done());

    done_bad(&mut acc, &TEST_INPUT[1..2])?;
    println!("{acc}");
    assert!(acc.is_done());

    Ok(())
}

#[test]
fn test_bad() -> Result<(), Box<dyn Error>> {
    println!("\nStarting the bad test");
    let mut acc = DecodeAccumulator::default();
    println!("{acc}");
    assert!(!acc.is_done());

    add_good(&mut acc, &TEST_INPUT[0..2], 1, 1)?;
    println!("{acc}");
    assert!(!acc.is_done());

    add_bad(&mut acc, &TEST_INPUT[1..2], 2, 1, &[209])?;
    println!("{acc}");

    Ok(())
}

#[rstest::rstest]
#[case(
    &[0x41, 0x21],
    &[0, 0],
    &[1, 2]
)]
#[case(
    &[0x2D, 0xD0, 0xB0, 0x20, 0xD0, 0xB1, 0x21, 0x0A],
    &[0, 1, 0, 0, 1, 0, 0, 0],
    &[1, 1, 2, 3, 3, 4, 5, 6]
)]
fn test_decode(
    #[case] data: &[u8],
    #[case] exp_buf: &[usize],
    #[case] exp_decoded: &[usize],
) -> Result<(), Box<dyn Error>> {
    let mut acc = DecodeAccumulator::default();
    println!("starting with {acc}");
    for ((byte, exp_buf_len), exp_decoded_len) in data
        .iter()
        .zip_eq(exp_buf.iter())
        .zip_eq(exp_decoded.iter())
    {
        println!("byte {byte} exp_buf_len {exp_buf_len} exp_decoded_len {exp_decoded_len}");
        add_good(&mut acc, &[*byte], *exp_buf_len, *exp_decoded_len)?;
        println!("{acc}");
    }
    Ok(())
}
